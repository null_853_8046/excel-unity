# ExcelUnity

#### 介绍
Excel,CSV文件 和用户的结构化对象之间的导入导出
支持将List<object>,datatable 对象导出到Excel或Csv文件
支持Excel和csv文件导入到 List<object>,datatable对象
支持导入的列绑定，唯一验证，格式验证，错误信息导出和提示
支持导出的单行多行合并，基本样式设置

#### 安装
Install-Package ExcelUnity -Version 1.0.0


service.AddExcelUnity();

#### 使用说明

最简单的导入示例：


```
public class ImportGrade
  {
    [ColumnName("年级名称")]
    public string GradeName { get; set; }

    [ColumnName("年级编码")]
    public string Code { get; set; }

    public int Other { get; set; }
}

  public void TestImportGrade()
 {
        var sheet = new ImportBookSheet(1, typeof(ImportGrade), 0);
        var bookmodel = new ImportBook(sheet);

        using var inputStream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excels//Export.xlsx"), FileMode.OpenOrCreate, FileAccess.Read);
        var importResult = _importer.Import(inputStream, FileType.Xlsx, bookmodel);

        var dataModels = importResult.GetDataModelAsList<ImportGrade>();
}
```


 

最简单的导出示例：

```
public void ExportToCsv()
{
    var list = new List<dynamic>();
    //usercode to fill list
    var sheet = new ExportSheet(list);
    var bookmodel = new ExportBook(sheet) { FileType = Core.FileType.Csv };
    using var outStream = new FileStream("D://Export_csv.csv", FileMode.Create, FileAccess.Write);
    _exporter.Export(bookmodel, outStream);
}
```
