﻿using ExcelUnity.Core;
using ExcelUnity.Exporter.Attributes;

namespace ExcelUnity.Test.Export.Dtos
{
    public class UserWithMergeData
    {
        [MergeRowAlone]
        public string Province { get; set; }

        [MergeRowAlone]
        public string City { get; set; }

        [PrimaryKey]
        [MergeRow]
        public int Id { get; set; }

        [MergeRow]
        public string Name { get; set; }

        [MergeRow]
        [ColumnName("年龄")]
        public int Age { get; set; }

        public string Hobby { get; set; }

        public string Sex { get; set; }
    }
}
