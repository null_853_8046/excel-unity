﻿using ExcelUnity.Core;
using ExcelUnity.Exporter.Attributes;
using ExcelUnity.Exporter.Attributes.Styles;
using ExcelUnity.Exporter.Styles;

namespace ExcelUnity.Test.Export.Dtos
{
    public class UserWithColumnStyle : DynamicCellStyle
    {
        [TestColumnStyle()]
        [TestHeaderStyle]
        public string Name { get; set; }

        [ColumnName("年龄")]
        [TestHeaderStyle(isBold: true)]
        [TestColumnStyle()]
        public int Age { get; set; }

        [TestHeaderStyle]
        [TestColumnStyle(isBold: true)]
        public string Hobby { get; set; }

        [IgnoreColumn]
        [TestColumnStyle()]
        [TestHeaderStyle]
        public string Sex { get; set; }
    }
}
