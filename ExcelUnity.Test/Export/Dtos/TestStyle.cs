﻿using ExcelUnity.Exporter.Attributes.Styles;
using ExcelUnity.Exporter.Styles;
using NPOI.SS.UserModel;
using System;

namespace ExcelUnity.Test.Export.Dtos
{
    public class TestStyle : BaseStyle
    {
        public bool SetBorder { get; set; }
        public override string GetCacheKey()
        {
            return base.GetCacheKey() + "_SetBorder" + SetBorder;
        }
        public override ICellStyle ToCellStyle(IWorkbook workBook)
        {
            var style = base.ToCellStyle(workBook);
            if (SetBorder)
            {
                style.BorderBottom = BorderStyle.Thin;
                style.BorderLeft = BorderStyle.Thin;
                style.BorderRight = BorderStyle.Thin;
                style.BorderTop = BorderStyle.Thin;
            }
            return style;
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class TestColumnStyleAttribute : ColumnStyleAttribute
    {
        public TestColumnStyleAttribute(bool isBold = false, bool wrapText = false, short fontColor = 8,
            int fontSize = 11, string fontName = "宋体", short fillForegroundColor = -1,
            HorizontalAlign horizontalAlign = HorizontalAlign.Left,
            Exporter.Styles.VerticalAlignment verticalAlign = Exporter.Styles.VerticalAlignment.Center,
            bool setborder=true)
             : base(isBold, wrapText, fontColor, fontSize, fontName, fillForegroundColor, horizontalAlign, verticalAlign)
        {
            ((TestStyle)Style).SetBorder = setborder;
        }
        public override BaseStyle CreateStyle()
        {
            return new TestStyle();
        }
    }
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class TestHeaderStyleAttribute : HeaderStyleAttribute
    {
        public TestHeaderStyleAttribute(bool isBold = true, bool wrapText = false, short fontColor = 8, int fontSize = 11, string fontName = "宋体", short fillForegroundColor = -1, HorizontalAlign horizontalAlign = HorizontalAlign.Center, Exporter.Styles.VerticalAlignment verticalAlign = Exporter.Styles.VerticalAlignment.Center,
            bool setborder = true)
             : base(isBold, wrapText, fontColor, fontSize, fontName, fillForegroundColor, horizontalAlign, verticalAlign)
        {
            ((TestStyle)Style).SetBorder = setborder;
        }
        public override BaseStyle CreateStyle()
        {
            return new TestStyle();
        }
    }
}
