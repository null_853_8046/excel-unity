﻿using ExcelUnity.Core;
using ExcelUnity.Exporter.Attributes;

namespace ExcelUnity.Test.Export.Dtos
{
    public class User
    {
        public string Name { get; set; }

        [ColumnName("年龄")]
        public int Age { get; set; }

        public string Hobby { get; set; }

        [IgnoreColumn]
        public string Sex { get; set; }
    }
}
