﻿using ExcelUnity.Exporter;
using ExcelUnity.Exporter.Exporters;
using ExcelUnity.Exporter.Styles;
using ExcelUnity.Test.Export.Dtos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace ExcelUnity.Test.Export
{
    public class ExportTest
    {
        protected readonly ITestOutputHelper Output;
        protected readonly IExcelExporter _exporter;
        private readonly List<User> _users;
        private readonly List<UserWithColumnStyle> _userWithStyles;
        public ExportTest(ITestOutputHelper output)
        {
            Output = output;
            _exporter = new DefaultExcelExporter();
            _users = Enumerable.Range(0, 100).Select(x => new User() { Age = x, Hobby = "Hobby" + x, Name = "Name" + x, Sex = "Sex" + x }).ToList();
            _userWithStyles = Enumerable.Range(0, 100).Select(x => new UserWithColumnStyle() { Age = x, Hobby = "Hobby" + x, Name = "Name" + x, Sex = "Sex" + x }).ToList();
        }

        [Fact]
        public void ExportUser()
        {
            var sheet = new ExportSheet(_users);
            var bookmodel = new ExportBook(sheet);

            using var outStream = new FileStream("D://Export_User.xlsx", FileMode.Create, FileAccess.Write);
            _exporter.Export(bookmodel, outStream);
        }

        [Fact]
        public void ExportUserWithTitle()
        {
            var sheet = new ExportSheet(_users)
            {
                Title = new ExportSheetTitle("用户列表")
                {
                    Style = new BaseStyle
                    {
                        IsBold = true,
                        HorizontalAlign = HorizontalAlign.Center
                    }
                }
            };
            var bookmodel = new ExportBook(sheet);

            using var outStream = new FileStream("D://Export_UserWithTitle.xlsx", FileMode.Create, FileAccess.Write);
            _exporter.Export(bookmodel, outStream);
        }

        [Fact]
        public void ExportUserWithColumnStyle()
        {
            var sheet = new ExportSheet(_userWithStyles)
            {
                Title = new ExportSheetTitle("用户列表")
                {
                    Style = new TestStyle
                    {
                        SetBorder = true,
                        IsBold = true,
                        HorizontalAlign = HorizontalAlign.Center
                    }
                }
            };
            var bookmodel = new ExportBook(sheet);

            using var outStream = new FileStream("D://Export_UserWithColumnStyle.xlsx", FileMode.Create, FileAccess.Write);
            _exporter.Export(bookmodel, outStream);
        }

        [Fact]
        public void ExportUserWithMergeData()
        {
            var users = Enumerable.Range(0, 100).Select(i =>
            {
                var x = new Random().Next(0, 50);
                return new UserWithMergeData()
                {
                    Province = "省份" + i / 20,
                    City = "城市" + i / 10,
                    Id = x,
                    Age = x,
                    Hobby = "Hobby" + x,
                    Name = "Name" + x,
                    Sex = "Sex" + x
                };
            }).OrderBy(x => x.Province).ThenBy(x => x.City).ThenBy(x => x.Id).ToList();
            var sheet = new ExportSheet(users)
            {
                Title = new ExportSheetTitle("用户列表")
                {
                    Style = new BaseStyle
                    {
                        IsBold = true,
                        HorizontalAlign = HorizontalAlign.Center
                    }
                },
                WaterMark = new ExportSheetWarterMark("私人文档")
            };
            var bookmodel = new ExportBook(sheet);

            using var outStream = new FileStream("D://Export_UserWithMergeData.xlsx", FileMode.Create, FileAccess.Write);
            _exporter.Export(bookmodel, outStream);
        }

        [Fact]
        public void ExportUserWithDynamicStyle()
        {
            var index = 0;
            foreach (var item in _userWithStyles)
            {
                if (index++ % 10 == 0)
                {
                    item.SetCellStyle(nameof(item.Name), new BaseStyle() { FontColor = 55 });
                }
            }
            var sheet = new ExportSheet(_userWithStyles)
            {
                Title = new ExportSheetTitle("用户列表")
                {
                    Style = new BaseStyle
                    {
                        IsBold = true,
                        HorizontalAlign = HorizontalAlign.Center
                    }
                },
            }.SetSpecialCellStyleByColumnName("年龄", "0", new BaseStyle() { FontColor = 100, IsBold = true, FontSize = 40 })
            .SetSpecialCellStyleByColumnName("年龄", "20", new BaseStyle() { FontColor = 5 });
            var bookmodel = new ExportBook(sheet);

            using var outStream = new FileStream("D://Export_UserWithDynamicStyle.xlsx", FileMode.Create, FileAccess.Write);
            _exporter.Export(bookmodel, outStream);
        }

        [Fact]
        public void ExportUserWithDataTable()
        {
            DataTable dt = new();
            dt.Columns.Add("RTime", typeof(string));
            dt.Columns.Add("Result", typeof(string));
            dt.Columns.Add("OName", typeof(string));
            dt.Columns.Add("Merge", typeof(string));
            for (int i = 0; i < 10; i++)
            {
                DataRow dr = dt.NewRow();
                dr["RTime"] = "RTime" + i;
                dr["OName"] = "OName" + i;
                dr["Merge"] = "Merge" + i / 3;
                dt.Rows.Add(dr);
            }
            var sheet = new ExportSheet(dt)
            {
                Title = new ExportSheetTitle("用户列表")
                {
                    Style = new BaseStyle
                    {
                        IsBold = true,
                        HorizontalAlign = HorizontalAlign.Center
                    }
                },
                MergeConfig = new ExportMergeConfig { MergeRowAloneByName = new List<string>() { "Merge" } },
                WaterMark = new ExportSheetWarterMark("技术部门--张利斯")
            };
            var bookmodel = new ExportBook(sheet);

            using var outStream = new FileStream("D://Export_Table.xlsx", FileMode.Create, FileAccess.Write);
            _exporter.Export(bookmodel, outStream);
        }

        [Fact]
        public void ExportUserWithDynamicType()
        {
            var list = new List<dynamic>();
            for (int i = 0; i < 10; i++)
            {
                dynamic dr = new ExpandoObject();
                dr.RTime = "RTime" + i;
                if (i > 4)
                {
                    dr.OName = "OName" + i;
                }
                if (i > 5)
                {
                    dr.Age = "Age" + i;
                    dr.Name = "Name" + i;
                }
                list.Add(dr);
            }
            var sheet = new ExportSheet(list)
            {
                Title = new ExportSheetTitle("用户列表")
                {
                    Style = new BaseStyle
                    {
                        IsBold = true,
                        HorizontalAlign = HorizontalAlign.Center
                    }
                }
            };
            var bookmodel = new ExportBook(sheet);

            using var outStream = new FileStream("D://Export_Dynamic.xlsx", FileMode.Create, FileAccess.Write);
            _exporter.Export(bookmodel, outStream);
        }

        [Fact]
        public void ExportMixType()
        {
            var list = new List<dynamic>();
            DataTable dt = new();
            dt.Columns.Add("RTime", typeof(string));
            dt.Columns.Add("Result", typeof(string));
            dt.Columns.Add("OName", typeof(string));
            for (int i = 0; i < 10; i++)
            {
                DataRow dr = dt.NewRow();
                dr["RTime"] = "RTime" + i;
                dr["OName"] = "OName" + i;
                dt.Rows.Add(dr);

                dynamic dy = new ExpandoObject();
                dy.RTime = "RTime" + i;
                if (i > 4)
                {
                    dy.OName = "OName" + i;
                }
                if (i > 5)
                {
                    dy.Age = "Age" + i;
                    dy.Name = "Name" + i;
                }
                list.Add(dy);
            }

            var sheet1 = new ExportSheet(dt)
            {
                Title = new ExportSheetTitle("用户列表")
                {
                    Style = new BaseStyle
                    {
                        IsBold = true,
                        HorizontalAlign = HorizontalAlign.Center
                    }
                }
            };
            var sheet2 = new ExportSheet(list)
            {
                Title = new ExportSheetTitle("用户列表2")
                {
                    Style = new BaseStyle
                    {
                        IsBold = true,
                        HorizontalAlign = HorizontalAlign.Center
                    }
                }
            };
            var sheet3 = new ExportSheet(_userWithStyles);
            var bookmodel = new ExportBook(sheet1, sheet2, sheet3);

            using var outStream = new FileStream("D://Export_MixType.xlsx", FileMode.Create, FileAccess.Write);
            _exporter.Export(bookmodel, outStream);
        }

        [Fact]
        public void ExportUserManySheet()
        {
            var sheet = new ExportSheet(_userWithStyles.Take(10).ToList())
            {
                SheetName = "first",
                Title = new ExportSheetTitle("用户列表")
                {
                    Style = new BaseStyle
                    {
                        IsBold = true,
                        HorizontalAlign = HorizontalAlign.Center
                    }
                }
            };

            DataTable dt = new();
            dt.Columns.Add("RTime", typeof(string));
            dt.Columns.Add("Result", typeof(string));
            dt.Columns.Add("OName", typeof(string));
            dt.Columns.Add("Merge", typeof(string));
            for (int i = 0; i < 10; i++)
            {
                DataRow dr = dt.NewRow();
                dr["RTime"] = "RTime" + i;
                dr["OName"] = "OName" + i;
                dr["Merge"] = "Merge" + i / 3;
                dt.Rows.Add(dr);
            }
            var sheet2 = new ExportSheet(dt)
            {
                SheetName = "first",
                Title = new ExportSheetTitle("用户列表2")
                {
                    Style = new BaseStyle
                    {
                        IsBold = true,
                        HorizontalAlign = HorizontalAlign.Center
                    }
                },
                MergeConfig = new ExportMergeConfig { MergeRowAloneByName = new List<string>() { "Merge" } },
                WaterMark = new ExportSheetWarterMark("技术部门--张利斯")
            };

            var users = Enumerable.Range(0, 100).Select(i =>
            {
                var x = new Random().Next(0, 50);
                return new UserWithMergeData()
                {
                    Province = "省份" + i / 20,
                    City = "城市" + i / 10,
                    Id = x,
                    Age = x,
                    Hobby = "Hobby" + x,
                    Name = "Name" + x,
                    Sex = "Sex" + x
                };
            }).OrderBy(x => x.Province).ThenBy(x => x.City).ThenBy(x => x.Id).ToList();
            var sheet3 = new ExportSheet(users)
            {
                Title = new ExportSheetTitle("用户列表")
                {
                    Style = new BaseStyle
                    {
                        IsBold = true,
                        HorizontalAlign = HorizontalAlign.Center
                    }
                },
                WaterMark = new ExportSheetWarterMark("私人文档")
            };
            var bookmodel = new ExportBook(sheet, sheet2, sheet3);

            using var outStream = new FileStream("D://Export_many_sheet.xlsx", FileMode.Create, FileAccess.Write);
            _exporter.Export(bookmodel, outStream);
        }


        [Fact]
        public void ExportToCsv()
        {
            var list = new List<dynamic>();
            for (int i = 0; i < 10; i++)
            {
                dynamic dr = new ExpandoObject();
                dr.RTime = "RTime" + i;
                if (i > 4)
                {
                    dr.OName = "OName" + i;
                }
                if (i > 5)
                {
                    dr.Age = "Age" + i;
                    dr.Name = "Name" + i;
                }
                list.Add(dr);
            }
            var sheet = new ExportSheet(list)
            {
                Title = new ExportSheetTitle("用户列表")
                {
                    Style = new BaseStyle
                    {
                        IsBold = true,
                        HorizontalAlign = HorizontalAlign.Center
                    }
                }
            };
            var bookmodel = new ExportBook(sheet) { FileType = Core.FileType.Csv };

            using var outStream = new FileStream("D://Export_csv.csv", FileMode.Create, FileAccess.Write);
            _exporter.Export(bookmodel, outStream);
        }
    }
}
