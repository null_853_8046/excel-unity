﻿using ExcelUnity.Core;

namespace ExcelUnity.Test.Imports.Dtos
{
    public class ImportSchool
    {
        [ColumnName("学校名称")]
        public string Name { get; set; }

        [ColumnName("学校地址")]
        public string Address { get; set; }

        [ColumnName("学费")]
        public double Price { get; set; }
    }
}
