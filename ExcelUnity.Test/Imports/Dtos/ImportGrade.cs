﻿using ExcelUnity.Core;

namespace ExcelUnity.Test.Imports.Dtos
{
    public class ImportGrade
    {
        [ColumnName("年级名称")]
        public string GradeName { get; set; }

        [ColumnName("年级编码")]
        public string Code { get; set; }

        public int Other { get; set; }
    }
}
