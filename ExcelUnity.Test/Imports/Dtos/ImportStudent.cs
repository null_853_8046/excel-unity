﻿using ExcelUnity.Core;
using ExcelUnity.Importer.Attributes;
using System;

namespace ExcelUnity.Test.Imports.Dtos
{
    public class ImportStudent
    {
        [ColumnName("Id")]
        public Guid Id { get; set; }

        [ColumnRequired("名字必填")]
        [ColumnName("名字")]
        public string Name { get; set; }
        [ColumnName("年龄")]
        public int Age { get; set; }

        [ColumnRequired]
        [ColumnName("生日")]
        public DateTime Birthday { get; set; }

        [ColumnName("入学时间")]
        public DateTime SchoolDate { get; set; }

        [ColumnUnique]
        [ColumnName("零花钱")]
        public double Money { get; set; }

        [ColumnName("电话")]
        [ColumnRegex(@"^[1]+[1-9]+\d{9}$", "电话格式不对")]
        public string Phone { get; set; }
    }

    public class ImportUserRelateRoleInput
    {
        public ImportUserRelateRoleInput()
        {
        }

        public ImportUserRelateRoleInput(string userName, string roleCodeStr)
        {
            UserName = userName;
            RoleCodeStr = roleCodeStr;
        }

        [ColumnIndex(0)]
        public string UserName { get; set; }

        [ColumnIndex(1)]
        public string RoleCodeStr { get; set; }
    }
}
