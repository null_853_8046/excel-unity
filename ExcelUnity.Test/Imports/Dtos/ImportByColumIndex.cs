﻿using ExcelUnity.Core;
using ExcelUnity.Importer.Attributes;

namespace ExcelUnity.Test.Imports.Dtos
{
    public class ImportByColumIndex
    {
        [ColumnIndex(2)]
        [ColumnUnique]
        public decimal? Free { get; set; }
    }
}
