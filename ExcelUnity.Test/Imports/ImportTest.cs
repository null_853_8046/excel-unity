﻿using ExcelUnity.Core;
using ExcelUnity.Importer;
using ExcelUnity.Importer.Importers;
using ExcelUnity.Importer.Wrappers;
using ExcelUnity.Test.Imports.Dtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Xunit;
using Xunit.Abstractions;

namespace ExcelUnity.Test.Imports
{
    public class ImportTest
    {
        protected readonly ITestOutputHelper Output;
        protected readonly IExcelImporter _importer;
        public ImportTest(ITestOutputHelper output)
        {
            Output = output;
            _importer = new DefaultExcelImporter();
        }

        [Fact]
        public void TestImportGrade()
        {
            var sheet = new ImportBookSheet(1, typeof(ImportGrade), 0);
            var bookmodel = new ImportBook(sheet);

            using var inputStream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excels//Export.xlsx"), FileMode.OpenOrCreate, FileAccess.Read);
            var importResult = _importer.Import(inputStream, FileType.Xlsx, bookmodel);

            var dataModels = importResult.GetDataModelAsList<ImportGrade>();

            Output.WriteImportResult(importResult, dataModels);
        }

        [Fact]
        public void TestImportStudent()
        {
            static void validateHandler(SheetWrapper wrapper, object objlist)
            {
                foreach (var model in (List<ImportStudent>)objlist)
                {
                    if (wrapper.IsValidatedData(model) && model.Name == "name0")
                        wrapper.SetError(model, nameof(model.Name), "名字不可为0");
                    if (wrapper.IsValidatedData(model) && model.Money < 0.5)
                        wrapper.SetError(model, nameof(model.Money), "零花钱不可小于0.5");
                }
            }

            var sheet = new ImportBookSheet(0, typeof(ImportStudent), 0)
            {
                UniqueValidationPrompt = "零花钱不可重复",
                ValidateHandler = validateHandler
            };
            var bookmodel = new ImportBook(sheet);

            using var inputStream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excels//Export.xlsx"), FileMode.OpenOrCreate, FileAccess.Read);
            var importResult = _importer.Import(inputStream, FileType.Xlsx, bookmodel);

            var dataModels = importResult.GetDataModelAsList<ImportStudent>();

            Output.WriteImportResult(importResult, dataModels);
        }

        [Fact]
        public void TestImportSchool()
        {
            static void validateHandler(SheetWrapper wrapper, object objlist)
            {
                foreach (var model in (List<ImportSchool>)objlist)
                {
                    if (model.Price > 0.5)
                        wrapper.SetError(model, nameof(model.Price), "学费不可大于0.5");
                }
            }

            var sheet = new ImportBookSheet(2, typeof(ImportSchool), 1)
            {
                UniqueValidationPrompt = "零花钱不可重复",
                ValidateHandler = validateHandler
            };
            var bookmodel = new ImportBook(sheet);

            using var inputStream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excels//Export.xlsx"), FileMode.OpenOrCreate, FileAccess.Read);
            var importResult = _importer.Import(inputStream, FileType.Xlsx, bookmodel);

            var dataModels = importResult.GetDataModelAsList<ImportSchool>();

            Output.WriteImportResult(importResult, dataModels);
        }

        [Fact]
        public void TestImportByColumIndex()
        {
            var sheet = new ImportBookSheet(0, typeof(ImportByColumIndex), 0) { ColumnMatchType = ColumnMatchType.ColumnIndex };
            var bookmodel = new ImportBook(sheet);

            using var inputStream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excels//Simple.xlsx"), FileMode.OpenOrCreate, FileAccess.Read);
            var importResult = _importer.Import(inputStream, FileType.Xlsx, bookmodel);

            var dataModels = importResult.GetDataModelAsList<ImportByColumIndex>();

            Output.WriteImportResult(importResult, dataModels);
        }

        [Fact]
        public void TestImportNoHeader()
        {
            var sheet = new ImportBookSheet(2, typeof(ImportByColumIndex), null) { ColumnMatchType = ColumnMatchType.ColumnIndex };
            var bookmodel = new ImportBook(sheet);

            using var inputStream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excels//Simple.xlsx"), FileMode.OpenOrCreate, FileAccess.Read);
            var importResult = _importer.Import(inputStream, FileType.Xlsx, bookmodel);

            var dataModels = importResult.GetDataModelAsList<ImportByColumIndex>();

            Output.WriteImportResult(importResult, dataModels);
        }

        [Fact]
        public void TestImportToDataTable()
        {
            static void validateHandler(SheetWrapper wrapper, object objlist)
            {
                var dt = (DataTable)objlist;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow item = dt.Rows[i];
                    if (wrapper.IsValidatedData(item) && string.IsNullOrWhiteSpace((item["运费"]?.ToString())))
                        wrapper.SetError(item, "运费", "运费不可为空");
                }
            }
            var sheet = new ImportBookSheet(0, typeof(DataTable), 0) { ColumnMatchType = ColumnMatchType.ColumnIndex, ValidateHandler = validateHandler };
            var bookmodel = new ImportBook(sheet);

            using var inputStream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excels//Simple.xlsx"), FileMode.OpenOrCreate, FileAccess.Read);
            {
                var importResult = _importer.Import(inputStream, FileType.Xlsx, bookmodel);
                var dt = importResult.GetDataModelAsDataTable();
                Output.WriteLine(importResult.SummaryError);
                Output.WriteLine(dt.Rows.Count.ToString());
            }

            var sheet2 = new ImportBookSheet(2, typeof(DataTable), null) { ColumnMatchType = ColumnMatchType.ColumnIndex };
            var bookmodel2 = new ImportBook(sheet2);
            using var inputStream2 = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excels//Simple.xlsx"), FileMode.OpenOrCreate, FileAccess.Read);
            {
                var importResult = _importer.Import(inputStream2, FileType.Xlsx, bookmodel2);
                var dt = importResult.GetDataModelAsDataTable();
                Output.WriteLine(dt.Rows.Count.ToString());
            }
        }

        [Fact]
        public void TestImportFromMergeExcel()
        {
            var sheet = new ImportBookSheet(0, typeof(DataTable), 0) { ColumnMatchType = ColumnMatchType.ColumnIndex, MergeCellUseEmptyValue = false };
            var bookmodel = new ImportBook(sheet);

            using var inputStream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excels//mergeexcel.xlsx"), FileMode.OpenOrCreate, FileAccess.Read);
            {
                var importResult = _importer.Import(inputStream, FileType.Xlsx, bookmodel);
                var dt = importResult.GetDataModelAsDataTable();
            }

        }

        [Fact]
        public void TestImportToDynamicList()
        {
            static void validateHandler(SheetWrapper wrapper, object objlist)
            {
                var list = (List<dynamic>)objlist;
                foreach (var item in list)
                {
                    if (wrapper.IsValidatedData(item) && string.IsNullOrWhiteSpace((string)(item.运费)))
                        wrapper.SetError(item, "运费", "运费不可为空");
                }
            }
            var sheet = new ImportBookSheet(10, typeof(object), 0) { ColumnMatchType = ColumnMatchType.ColumnIndex, ValidateHandler = validateHandler };
            var bookmodel = new ImportBook(sheet);

            using var inputStream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excels//Simple.xlsx"), FileMode.OpenOrCreate, FileAccess.Read);
            {
                var importResult = _importer.Import(inputStream, FileType.Xlsx, bookmodel);
                var dt = importResult.GetDataModelAsList<dynamic>();
                Output.WriteImportResult(importResult, dt);
            }

            var sheet2 = new ImportBookSheet(2, typeof(object), null) { ColumnMatchType = ColumnMatchType.ColumnIndex };
            var bookmodel2 = new ImportBook(sheet2);
            using var inputStream2 = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excels//Simple.xlsx"), FileMode.OpenOrCreate, FileAccess.Read);
            {
                var importResult = _importer.Import(inputStream2, FileType.Xlsx, bookmodel2);
                var dt = importResult.GetDataModelAsList<dynamic>();
                Output.WriteImportResult(importResult, dt);
            }
        }

        [Fact]
        public void TestImportStudentWithErrorExcel()
        {
            static void validateHandler(SheetWrapper wrapper, object objlist)
            {
                foreach (var model in (List<ImportStudent>)objlist)
                {
                    if (wrapper.IsValidatedData(model) && model.Name == "name0")
                        wrapper.SetError(model, nameof(model.Name), "名字不可为0");
                    if (wrapper.IsValidatedData(model) && model.Money < 0.5)
                        wrapper.SetError(model, nameof(model.Money), "零花钱不可小于0.5");
                }
            }

            var sheet = new ImportBookSheet(0, typeof(ImportStudent))
            {
                UniqueValidationPrompt = "零花钱不可重复",
                ValidateHandler = validateHandler
            };
            var bookmodel = new ImportBook(sheet);

            using var inputStream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excels//Export.xlsx"), FileMode.OpenOrCreate, FileAccess.Read);
            using var outStream = new FileStream("D://Simple_Error.xlsx", FileMode.Create, FileAccess.Write);
            var importResult = _importer.Import(inputStream, FileType.Xlsx, bookmodel, outStream);

            var dataModels = importResult.GetDataModelAsList<ImportStudent>();

            Output.WriteImportResult(importResult, dataModels);
        }

        [Fact]
        public void TestImportToDynamicListByCsv()
        {
            static void validateHandler(SheetWrapper wrapper, object objlist)
            {
                var list = (List<dynamic>)objlist;
                foreach (var item in list)
                {
                    if (wrapper.IsValidatedData(item) && string.IsNullOrWhiteSpace(((string)(item.运费))))
                        wrapper.SetError(item, "运费", "运费不可为空");
                }
            }
            var sheet = new ImportBookSheet(0, typeof(object), 0) { ColumnMatchType = ColumnMatchType.ColumnIndex, ValidateHandler = validateHandler };
            var bookmodel = new ImportBook(sheet);

            using var inputStream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excels//Simple.csv"), FileMode.OpenOrCreate, FileAccess.Read);
            {
                using var outStream = new FileStream("D://Simple_Error.csv", FileMode.Create, FileAccess.Write);
                var importResult = _importer.Import(inputStream, FileType.Csv, bookmodel, outStream);
                var dt = importResult.GetDataModelAsList<dynamic>();
                Output.WriteImportResult(importResult, dt);
            }
        }

        [Fact]
        public void TestImportToImportUserRelateRoleInputByCsv()
        {
            using var inputStream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excels//用户角色关联模板 (1).csv"), FileMode.OpenOrCreate, FileAccess.Read);
            {
                var importResult = _importer.Import(inputStream, FileType.Csv, new ImportBook(new ImportBookSheet(typeof(ImportUserRelateRoleInput)) { ColumnMatchType = ColumnMatchType.ColumnIndex }));

                var dt = importResult.GetDataModelAsList<ImportUserRelateRoleInput>();
                Output.WriteImportResult(importResult, dt);
            }
        }
    }
}
