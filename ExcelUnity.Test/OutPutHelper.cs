﻿using ExcelUnity.Importer.Results;
using Newtonsoft.Json;
using System;
using Xunit.Abstractions;

namespace ExcelUnity.Test
{
    public static class OutPutHelper
    {

        public static void WriteImportResult(this ITestOutputHelper Output, ImportResult result, object dataModels)
        {
            Output.WriteLine($"success:{result.ImportSuccess}");
            Output.WriteLine($"summaryErrorMsg-----{Environment.NewLine}{result.SummaryError}");
            Output.WriteLine($"notDisplayMsg-----{Environment.NewLine}{result.NotDisplayError}");
            Output.WriteLine($"dataModels-----{Environment.NewLine}{(dataModels == null ? null : JsonConvert.SerializeObject(dataModels))}");
        }
    }
}
