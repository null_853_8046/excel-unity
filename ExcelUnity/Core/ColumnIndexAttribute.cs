﻿using System;

namespace ExcelUnity.Core
{
    /// <summary>
    /// 列索引
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class ColumnIndexAttribute : Attribute
    {
        public ColumnIndexAttribute(int index)
        {
            Index = index;
        }

        public int Index { get; set; }
    }
}
