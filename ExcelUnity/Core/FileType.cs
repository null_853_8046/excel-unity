﻿namespace ExcelUnity.Core
{
    /// <summary>
    /// Excel类型
    /// </summary>
    public enum FileType
    {
        /// <summary>
        /// XLSX
        /// </summary>
        Xlsx = 1,

        /// <summary>
        /// XLS
        /// </summary>
        Xls = 2,

        /// <summary>
        /// csv
        /// </summary>
        Csv = 3,
    }
}
