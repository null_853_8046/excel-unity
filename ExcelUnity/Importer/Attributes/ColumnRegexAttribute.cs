﻿using System;
using System.Text.RegularExpressions;

namespace ExcelUnity.Importer.Attributes
{
    /// <summary>
    /// 正则判断
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class ColumnRegexAttribute : Attribute
    {
        public ColumnRegexAttribute(string pattern)
        {
            Pattern = pattern;
        }

        public ColumnRegexAttribute(string pattern, string errorMessage)
            : this(pattern)
        {
            Pattern = pattern;
            ErrorMessage = errorMessage;
        }

        public ColumnRegexAttribute(string pattern, string errorMessage, RegexOptions regexOptions)
            : this(pattern, errorMessage)
        {
            RegexOptions = regexOptions;
        }

        public string Pattern { get; set; }

        public RegexOptions RegexOptions { get; set; }

        public string ErrorMessage { get; set; } = "数据格式不正确";
    }
}
