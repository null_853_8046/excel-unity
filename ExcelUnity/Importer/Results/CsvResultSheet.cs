﻿namespace ExcelUnity.Importer.Results
{
    public class CsvResultSheet : ResultSheet
    {
        public CsvResultSheet(bool isValidated, string? sheetError, string summaryError, object? dataModel) : base(0, string.Empty, isValidated, sheetError, summaryError, dataModel)
        {
        }

        public override string SheetDisplayName => string.Empty;
    }
}
