﻿using ExcelUnity.Core;
using System.Collections.Generic;
using System.Data;

namespace ExcelUnity.Importer.Results
{
    public class ResultSheet
    {
        public ResultSheet(int sheetIndex, string? sheetName, bool isValidated, string? sheetError, string summaryError, object? dataModel)
        {
            SheetIndex = sheetIndex;
            SheetName = sheetName;
            IsValidated = isValidated;
            SheetError = sheetError;
            SummaryError = summaryError;
            DataModel = dataModel;
        }

        public ResultSheet(int sheetIndex, string? sheetName, string sheetError)
        {
            SheetIndex = sheetIndex;
            SheetName = sheetName;
            IsValidated = false;
            SheetError = sheetError;
            SummaryError = sheetError;
        }

        public virtual string SheetDisplayName => SheetName.IsNotNullOrWhiteSpace() ? $"Sheet_Name:{SheetName}" : $"Sheet_Index:{SheetIndex}";

        public int SheetIndex { get; }

        public string? SheetName { get; }

        /// <summary>
        /// 当前sheet是否校验成功
        /// </summary>
        public bool IsValidated { get; }

        /// <summary>
        /// sheet格式错误信息
        /// </summary>
        public string? SheetError { get; }

        /// <summary>
        /// 错误统计信息
        /// </summary>
        public string SummaryError { get; }

        public object? DataModel { get; }

        public List<T> GetDataModelAsList<T>()
        {
            if (DataModel == null) return new List<T>();

            return (List<T>)DataModel;
        }

        public DataTable GetDataModelAsDataTable()
        {
            if (DataModel == null) return new DataTable();

            return (DataTable)DataModel;
        }
    }
}
