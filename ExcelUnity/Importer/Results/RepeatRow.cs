﻿using System.Collections.Generic;

namespace ExcelUnity.Importer.Results
{
    /// <summary>
    /// 重复行
    /// </summary>
    public class RepeatRow
    {
        public RepeatRow(int rowIndex, List<int> columnIndexes)
        {
            RowIndex = rowIndex;
            ColumnIndexes = columnIndexes;
        }

        public int RowIndex { get; }

        public List<int> ColumnIndexes { get; }

        /// <summary>
        /// 展示出来的行号
        /// </summary>
        public int DisplayRowIndex => RowIndex + 1;
    }
}
