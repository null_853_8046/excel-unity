﻿using ExcelUnity.Core;
using ExcelUnity.Importer.Importers;
using ExcelUnity.Importer.Wrappers.SheetDataBuilders;
using NPOI.SS.UserModel;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Linq;

namespace ExcelUnity.Importer.Wrappers.SheetWraperBuilders
{
    public class ExcelSheetWrapperBuilder
    {
        public static List<SheetWrapper> BuildSheetWrappersByExcel(ImportBook importBook, IWorkbook workBook)
        {
            var sheetWrappers = new List<SheetWrapper>();
            foreach (var importSheet in importBook.Sheets)
            {
                ISheet? sheet;
                if (importSheet.SheetMatchType == SheetMatchType.SheetIndex)
                    sheet = workBook.GetSheetOrNull(importSheet.SheetIndex);
                else
                    sheet = workBook.GetSheetOrNull(importSheet.SheetName!);
                if (sheet == null)
                    continue;

                var (columnWrappers, columnError) = BuildColumnWrappers(sheet, importSheet);
                if (columnError.IsNotNullOrWhiteSpace())
                {
                    sheetWrappers.Add(new SheetWrapper(importSheet, workBook.GetSheetIndex(sheet), sheet.SheetName) { SheetError = columnError });
                    continue;
                }

                var sheetWrapper = BuildSheetWrapper(sheet, workBook.GetSheetIndex(sheet), importSheet, columnWrappers!);

                sheetWrapper.ModelData = SheetDataModelBuilder.BuildSheetDataModel(sheetWrapper, importSheet.DataType);

                sheetWrapper.Validate();
                sheetWrappers.Add(sheetWrapper);
            }

            return sheetWrappers;
        }

        private static (List<ColumnWrapper>? ColumnWrappers, string? ErrorMessage) BuildColumnWrappers(ISheet sheet, ImportBookSheet importSheet)
        {
            if (sheet.LastRowNum == 0) return (null, "表格没有数据");

            IRow? columnRow = null;
            if (importSheet.HeaderRowIndex.HasValue)
            {
                if (sheet.LastRowNum >= importSheet.HeaderRowIndex)
                    columnRow = sheet.GetRow(importSheet.HeaderRowIndex.Value);
                else
                    return (null, "未找到表头行");
            }
            else
            {
                columnRow = sheet.GetRow(sheet.FirstRowNum); ;
            }

            if (columnRow.IsEmptyOrNull())
                return (new List<ColumnWrapper>(), importSheet.HeaderRowIndex == null ? "表格没有数据" : "未找到表头行");

            var columnWrappers = Enumerable.Range(columnRow!.FirstCellNum, columnRow.LastCellNum - columnRow.FirstCellNum)
                                           .Select(x => new ColumnWrapper(x, importSheet.HeaderRowIndex.HasValue ? columnRow.GetCell(x).GetValue() : null))
                                           .ToList();

            if (columnWrappers.IsNullOrEmpty())
                return (columnWrappers, importSheet.HeaderRowIndex == null ? "表格没有数据" : "未找到表头行");

            var bindError = ColumnWrapperBinder.BindColumWrapperToDataType(columnWrappers, importSheet.DataType, importSheet.ColumnMatchType);
            return (columnWrappers, bindError);
        }



        private static SheetWrapper BuildSheetWrapper(ISheet sheet, int sheetIndex, ImportBookSheet importSheet, List<ColumnWrapper> columnWrappers)
        {
            var sheetWrapper = new SheetWrapper(importSheet, sheetIndex, sheet.SheetName)
            {
                Columns = columnWrappers,
                DataRowStartIndex = importSheet.HeaderRowIndex.HasValue ? importSheet.HeaderRowIndex.Value + 1 : sheet.FirstRowNum,
                Rows = new List<RowWrapper>()
            };


            var minColumnIndex = sheetWrapper.Columns.Min(x => x.ColumnIndex);
            var maxColumnIndex = sheetWrapper.Columns.Max(x => x.ColumnIndex);

            var mergeCellMap = GetMergeCellMapping(sheet, maxColumnIndex);

            for (int rowIndex = sheetWrapper.DataRowStartIndex.Value; rowIndex <= sheet.LastRowNum; rowIndex++)
            {
                var row = sheet.GetRow(rowIndex);
                if (row.IsEmptyOrNull(minColumnIndex, maxColumnIndex))
                    continue;
                var cellWrappers = sheetWrapper.Columns.Select(x =>
                {
                    var cell = row.GetCell(x.ColumnIndex);
                    var cellValue = cell.GetValue();
                    if (!importSheet.MergeCellUseEmptyValue && cell != null && cell.IsMergedCell)
                        cellValue = mergeCellMap[rowIndex, x.ColumnIndex].GetValue();

                    return new CellWrapper(x.ColumnIndex, cellValue, x.IsUnique, x.PropertyName)
                    {
                        IsMergedCell = cell?.IsMergedCell ?? false
                    };
                }).ToList();
                sheetWrapper.Rows.Add(new RowWrapper(rowIndex, cellWrappers));
            }

            return sheetWrapper;
        }

        /// <summary>
        /// 获取合并单元格映射
        /// </summary>
        /// <param name="sheet"></param>
        /// <returns>rowIndex,columnIndex,FirstCell</returns>
        private static ICell[,] GetMergeCellMapping(ISheet sheet, int maxColumnIndex)
        {
            var mapping = new ICell[sheet.LastRowNum + 1, maxColumnIndex + 1];
            for (int i = 0; i < sheet.NumMergedRegions; i++)//遍历所有的合并单元格
            {
                var cellRange = sheet.GetMergedRegion(i);
                var firstCell = sheet.GetRow(cellRange.FirstRow).GetCell(cellRange.FirstColumn);

                for (int rowIndex = cellRange.FirstRow; rowIndex <= cellRange.LastRow; rowIndex++)
                {
                    for (int colIndex = cellRange.FirstColumn; colIndex <= cellRange.LastColumn; colIndex++)
                    {
                        mapping[rowIndex, colIndex] = firstCell;
                    }
                }
            }
            return mapping;
        }
    }
}
