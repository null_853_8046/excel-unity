﻿using ExcelUnity.Core;
using ExcelUnity.Importer.Importers;
using ExcelUnity.Importer.Wrappers.SheetDataBuilders;
using System.Collections.Generic;
using System.Linq;

namespace ExcelUnity.Importer.Wrappers.SheetWraperBuilders
{
    public class CsvSheetWrapperBuilder
    {
        public static CsvSheetWrapper BuildSheetWrapperByCsv(ImportBookSheet importSheet, CsvSheet sheet)
        {
            var (columnWrappers, columnError) = BuildColumnWrappers(importSheet, sheet);
            if (columnError.IsNotNullOrWhiteSpace())
                return new CsvSheetWrapper(importSheet, columnError!);

            var sheetWrapper = new CsvSheetWrapper(importSheet)
            {
                Columns = columnWrappers,
                DataRowStartIndex = importSheet.HeaderRowIndex.HasValue ? importSheet.HeaderRowIndex.Value + 1 : sheet.FirstRowIndex,
                Rows = new List<RowWrapper>()
            };

            var minColumnIndex = sheetWrapper.Columns.Min(x => x.ColumnIndex);
            var maxColumnIndex = sheetWrapper.Columns.Max(x => x.ColumnIndex);
            for (int rowIndex = sheetWrapper.DataRowStartIndex.Value; rowIndex < sheet.RowNum; rowIndex++)
            {
                var row = sheet.GetRow(rowIndex);
                if (row.IsEmpty(minColumnIndex, maxColumnIndex)) continue;

                var cellWrappers = sheetWrapper.Columns.Select(x => new CellWrapper(x.ColumnIndex, row.GetCellValueOrEmpty(x.ColumnIndex), x.IsUnique, x.PropertyName)).ToList();
                sheetWrapper.Rows.Add(new RowWrapper(rowIndex, cellWrappers));
            }
            sheetWrapper.ModelData = SheetDataModelBuilder.BuildSheetDataModel(sheetWrapper, importSheet.DataType);

            sheetWrapper.Validate();
            return sheetWrapper;
        }

        public static (List<ColumnWrapper>? ColumnWrappers, string? Error) BuildColumnWrappers(ImportBookSheet importSheet, CsvSheet sheet)
        {
            if (sheet.RowNum == 0) return (null, "表格没有数据");

            CsvRow? columnRow = null;
            if (importSheet.HeaderRowIndex.HasValue)
            {
                if (sheet.Rows.Count > importSheet.HeaderRowIndex)
                    columnRow = sheet.GetRow(importSheet.HeaderRowIndex.Value);
                else
                    return (null, "未找到表头行");
            }
            else
            {
                columnRow = sheet.GetRow(sheet.FirstRowIndex);
            }

            if (columnRow.IsEmpty())
                return (null, importSheet.HeaderRowIndex == null ? "表格没有数据" : "未找到表头行");

            var columnWrappers = Enumerable.Range(0, columnRow.CellCount)
                                           .Select(x => new ColumnWrapper(x, importSheet.HeaderRowIndex.HasValue ? columnRow.GetCell(x).Value : null))
                                           .ToList();
            if (columnWrappers.IsNullOrEmpty())
                return (columnWrappers, importSheet.HeaderRowIndex == null ? "表格没有数据" : "未找到表头行");

            var bindError = ColumnWrapperBinder.BindColumWrapperToDataType(columnWrappers!, importSheet.DataType, importSheet.ColumnMatchType);
            return (columnWrappers, bindError);
        }
    }
}
