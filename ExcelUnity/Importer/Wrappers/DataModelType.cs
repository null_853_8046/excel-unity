﻿namespace ExcelUnity.Importer.Wrappers
{
    public enum DataModelType
    {
        CustomerType,
        DataTable,
        Dynamic
    }
}
