﻿using ExcelUnity.Importer.Importers;

namespace ExcelUnity.Importer.Wrappers
{
    public class CsvSheetWrapper : SheetWrapper
    {
        public CsvSheetWrapper(ImportBookSheet importSheet) : base(importSheet, 0, string.Empty)
        {
        }

        public CsvSheetWrapper(ImportBookSheet importSheet, string sheetError) : base(importSheet, 0, string.Empty)
        {
            SheetError = sheetError;
        }
    }
}
