﻿using ExcelUnity.Core;

namespace ExcelUnity.Importer.Wrappers
{
    public class CellWrapper
    {
        public CellWrapper(int columnIndex, string value, bool isUnique, string? propertyName)
        {
            ColumnIndex = columnIndex;
            Value = value;
            IsUnique = isUnique;
            PropertyName = propertyName;
        }

        /// <summary>
        /// 单元格列索引
        /// </summary>
        public int ColumnIndex { get; set; }

        /// <summary>
        /// 单元格内容，不可为空，空为string.empty
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 是否参与唯一值判断
        /// </summary>
        public bool IsUnique { get; set; }

        /// <summary>
        /// 对应的属性名
        /// </summary>
        public string? PropertyName { get; set; }

        /// <summary>
        /// 单元格错误信息
        /// </summary>
        public string? Error { get; set; }

        /// <summary>
        /// 是否是合并单元格
        /// </summary>
        public bool IsMergedCell { get; set; }

        public void AppendError(string errorMsg)
        {
            if (Error.IsNullOrWhiteSpace())
                Error = errorMsg;
            else
                Error += $",{errorMsg}";
        }
    }
}
