﻿using ExcelUnity.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ExcelUnity.Importer.Wrappers
{
    public class RowWrapper
    {
        public RowWrapper(int rowIndex, List<CellWrapper> cells)
        {
            RowIndex = rowIndex;
            Cells = cells;

            if (!cells.Any(x => x.IsUnique))
                UniqueSign = Guid.NewGuid().ToString();
            else
                UniqueSign = string.Join("_", cells.Where(x => x.IsUnique).Select(x => x.Value));
        }

        public object? ModelData { get; set; }

        /// <summary>
        /// 数据所在的excel行索引
        /// </summary>
        public int RowIndex { get; }

        /// <summary>
        /// 行所包含的单元格
        /// </summary>
        public List<CellWrapper> Cells { get; set; }

        /// <summary>
        /// 数据唯一标识
        /// </summary>
        public string? UniqueSign { get; set; }

        /// <summary>
        /// 字段在excel中列坐标和错误
        /// </summary>
        public Dictionary<int, string>? ColumnErrors { get; private set; }

        /// <summary>
        /// 当前row是否校验成功
        /// </summary>
        public bool IsValidated => Cells.All(x => x.Error.IsNullOrWhiteSpace());
    }
}
