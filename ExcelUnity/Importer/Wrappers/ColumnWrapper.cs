﻿namespace ExcelUnity.Importer.Wrappers
{
    public class ColumnWrapper
    {
        public ColumnWrapper(int columnIndex, string? columnName)
        {
            ColumnIndex = columnIndex;
            ColumnName = columnName ?? "CELL_" + columnIndex;
        }

        /// <summary>
        /// 列索引
        /// </summary>
        public int ColumnIndex { get; set; }

        /// <summary>
        /// 列名
        /// </summary>
        public string ColumnName { get; set; }

        public ColumnProperty? ColumnProperty { get; set; }

        public bool IsUnique => ColumnProperty?.IsUnique ?? false;

        public string? PropertyName { get; set; }
    }
}
