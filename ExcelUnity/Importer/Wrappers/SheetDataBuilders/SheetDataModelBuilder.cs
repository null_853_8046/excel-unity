﻿using System;
using System.Data;

namespace ExcelUnity.Importer.Wrappers.SheetDataBuilders
{
    public class SheetDataModelBuilder
    {
        public static object BuildSheetDataModel(SheetWrapper sheet, Type dataType)
        {
            if (dataType.FullName == typeof(DataTable).FullName)
                return DataTableBuilder.CreateDataTable(sheet.Rows, sheet.Columns);
            else if (dataType.FullName == "System.Object")
                return DynamicDataBuilder.CreateDynamicDataList(sheet.Rows, sheet.Columns);
            else
                return DefaultDataBuilder.CreateDefaultDataList(sheet.Rows, sheet.Columns, dataType);

        }
    }
}
