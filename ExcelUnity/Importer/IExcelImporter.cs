﻿using ExcelUnity.Core;
using ExcelUnity.Importer.Importers;
using ExcelUnity.Importer.Results;
using System.IO;

namespace ExcelUnity.Importer
{
    public interface IExcelImporter
    {
        /// <summary>
        /// csv或excel文件导入
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="importBook"></param>
        /// <param name="outPutErrorStream"></param>
        /// <returns></returns>
        ImportResult Import(string filePath, ImportBook importBook, Stream? outPutErrorStream = null);

        /// <summary>
        /// csv或excel文件流导入
        /// </summary>
        /// <param name="fileStream">导入文件流</param>
        /// <param name="ext">文件类型</param>
        /// <param name="importBook">导入模型</param>
        /// <param name="outPutErrorStream">错误输出流</param>
        /// <returns>导入结果</returns>
        ImportResult Import(Stream fileStream, FileType fileType, ImportBook importBook, Stream? outPutErrorStream = null);

        /// <summary>
        /// csv文件导入
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="importBook"></param>
        /// <param name="outPutErrorStream"></param>
        /// <returns></returns>
        ImportResult ImportCsv(string filePath, ImportBook importBook, Stream? outPutErrorStream = null);

        /// <summary>
        /// csv文件流导入
        /// </summary>
        /// <param name="fileStream"></param>
        /// <param name="importBook"></param>
        /// <param name="outPutErrorStream"></param>
        /// <returns></returns>
        ImportResult ImportCsv(Stream fileStream, ImportBook importBook, Stream? outPutErrorStream);
    }
}
