﻿using ExcelUnity.Core;
using ExcelUnity.Importer.Wrappers;
using System;

namespace ExcelUnity.Importer.Importers
{
    public class ImportBookSheet
    {
        public ImportBookSheet(Type dataType)
        {
            DataType = dataType;
        }

        public ImportBookSheet(int sheetIndex, Type dataType, int? headerRowIndex = 0)
        {
            HeaderRowIndex = headerRowIndex;
            SheetIndex = sheetIndex;
            DataType = dataType;
        }

        public int SheetIndex { get; set; } = 0;

        public string? SheetName { get; set; } = null;

        public SheetMatchType SheetMatchType => SheetName.IsNotNullOrWhiteSpace() ? SheetMatchType.SheetName : SheetMatchType.SheetIndex;
        /// <summary>
        /// 表头所在行
        /// </summary>
        public int? HeaderRowIndex { get; set; } = 0;

        /// <summary>
        /// 是否包含表头
        /// </summary>
        public bool HasHeaderRow => HeaderRowIndex.HasValue;

        /// <summary>
        /// 是否需要唯一验证
        /// </summary>
        public bool NeedUniqueValidation { get; set; } = true;

        /// <summary>
        /// 唯一验证提示
        /// </summary>
        public string UniqueValidationPrompt { get; set; } = "数据重复";

        /// <summary>
        /// 承载数据类型
        /// </summary>
        public Type DataType { get; private set; }

        public ColumnMatchType ColumnMatchType { get; set; } = ColumnMatchType.ColumnName;

        /// <summary>
        /// 自定义校验委托
        /// </summary>
        /// </summary>
        public Action<SheetWrapper, object>? ValidateHandler { get; set; }

        /// <summary>
        /// 合并单元格使用空数据
        /// </summary>
        public bool MergeCellUseEmptyValue { get; set; } = false;

        public bool IsMatchSheet(int sheetIndex, string sheetName)
        {
            return SheetMatchType == SheetMatchType.SheetName ? sheetName == SheetName : sheetIndex == SheetIndex;
        }

        public string? UserParamValidate()
        {
            if (ColumnMatchType == ColumnMatchType.ColumnName && !HasHeaderRow)
                return "列名匹配必须包含表头";

            return null;
        }
    }
}
