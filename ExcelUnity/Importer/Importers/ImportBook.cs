﻿using NPOI.HSSF.Util;
using System.Collections.Generic;
using System.Linq;

namespace ExcelUnity.Importer.Importers
{
    public class ImportBook
    {
        public ImportBook(params ImportBookSheet[] sheets)
        {
            Sheets = sheets.ToList();
        }

        public ErrorStyle? ErrorStyle { get; set; }

        public List<ImportBookSheet> Sheets { get; set; }
    }

    public class ErrorStyle
    {
        /// <summary>
        /// 错误前景色
        /// </summary>
        public short DataErrorForegroundColor { get; set; } = HSSFColor.Red.Index;

        /// <summary>
        /// 重复前景色
        /// </summary>
        public short RepeatedErrorForegroundColor { get; set; } = HSSFColor.Yellow.Index;

        /// <summary>
        /// 默认前景色
        /// </summary>
        public short DefaultForegroundColor { get; set; } = HSSFColor.White.Index;
    }
}
