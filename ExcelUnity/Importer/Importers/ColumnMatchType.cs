﻿namespace ExcelUnity.Importer.Importers
{
    /// <summary>
    /// 列匹配方式
    /// </summary>
    public enum ColumnMatchType
    {
        /// <summary>
        /// 列明匹配
        /// </summary>
        ColumnName,

        /// <summary>
        /// 列索引匹配
        /// </summary>
        ColumnIndex
    }
}
