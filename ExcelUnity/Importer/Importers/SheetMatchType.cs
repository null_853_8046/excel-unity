﻿namespace ExcelUnity.Importer.Importers
{
    /// <summary>
    /// sheet匹配方式
    /// </summary>
    public enum SheetMatchType
    {
        SheetIndex,
        SheetName
    }
}
