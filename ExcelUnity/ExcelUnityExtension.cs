﻿using ExcelUnity.Exporter;
using ExcelUnity.Importer;
using Microsoft.Extensions.DependencyInjection;

namespace ExcelUnity
{
    public static class ExcelUnityExtension
    {
        public static IServiceCollection AddExcelUnity(this IServiceCollection service)
        {
            service.AddSingleton(typeof(IExcelExporter), typeof(DefaultExcelExporter));
            service.AddSingleton(typeof(IExcelImporter), typeof(DefaultExcelImporter));
            return service;
        }
    }
}
