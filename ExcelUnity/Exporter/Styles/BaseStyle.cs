﻿using NPOI.SS.UserModel;
using System.Text;

namespace ExcelUnity.Exporter.Styles
{
    public class BaseStyle
    {
        public BaseStyle()
        {
        }

        public BaseStyle(bool isBold, bool wrapText, short fontColor, int fontSize, string fontName, short? fillForegroundColor, HorizontalAlign horizontalAlign, VerticalAlignment verticalAlign)
        {
            IsBold = isBold;
            WrapText = wrapText;
            FontColor = fontColor;
            FontSize = fontSize;
            FontName = fontName;
            FillForegroundColor = fillForegroundColor;
            HorizontalAlign = horizontalAlign;
            VerticalAlign = verticalAlign;
        }

        /// <summary>
        /// 加粗
        /// </summary>
        public bool IsBold { get; set; } = false;

        /// <summary>
        /// 自动换行
        /// </summary>
        public bool WrapText { get; set; } = false;

        public short FontColor { get; set; } = 8;

        public int FontSize { get; set; } = 11;

        /// <summary>
        /// 字体名称
        /// </summary>
        public string FontName { get; set; } = "宋体";

        public short? FillForegroundColor { get; set; }

        public HorizontalAlign HorizontalAlign { get; set; } = HorizontalAlign.Left;

        public VerticalAlignment VerticalAlign { get; set; } = VerticalAlignment.Center;

        public virtual string GetCacheKey()
        {
            var str = new StringBuilder();
            foreach (var item in GetType().GetProperties())
            {
                str.Append(item.Name.ToString()).Append("_").Append(item.GetValue(this)?.ToString() ?? string.Empty).Append("__");
            }
            return str.ToString();
        }

        public virtual ICellStyle? ToCellStyle(IWorkbook workBook)
        {
            var cellStyle = workBook.CreateCellStyle();
            cellStyle.Alignment = (HorizontalAlignment)HorizontalAlign;
            var font = workBook.CreateFont();
            font.IsBold = IsBold;
            font.FontName = FontName;
            font.FontHeightInPoints = FontSize;
            font.Color = FontColor;
            cellStyle.SetFont(font);
            cellStyle.VerticalAlignment = (NPOI.SS.UserModel.VerticalAlignment)VerticalAlign;
            cellStyle.Alignment = (HorizontalAlignment)HorizontalAlign;
            cellStyle.WrapText = WrapText;
            if (FillForegroundColor.HasValue)
            {
                cellStyle.FillPattern = FillPattern.SolidForeground;
                cellStyle.FillForegroundColor = FillForegroundColor.Value;
            }
            return cellStyle;
        }

    }

    public enum HorizontalAlign
    {
        Left = 1,
        Center = 2,
        Right = 3,
    }

    public enum VerticalAlignment
    {
        Top = 0,
        Center = 1,
        Bottom = 2,
    }
}
