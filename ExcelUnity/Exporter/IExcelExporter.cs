﻿using ExcelUnity.Exporter.Exporters;
using System.IO;

namespace ExcelUnity.Exporter
{
    public interface IExcelExporter
    {
        /// <summary>
        /// 导出到文件
        /// </summary>
        /// <param name="book"></param>
        /// <param name="filePath"></param>
        void Export(ExportBook book, string filePath);

        /// <summary>
        /// 导出到文件流
        /// </summary>
        /// <param name="book"></param>
        /// <param name="stream"></param>
        void Export(ExportBook book, Stream stream);

        /// <summary>
        /// 导出返回字节数组
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        byte[] Export(ExportBook book);
    }
}
