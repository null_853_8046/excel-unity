﻿using ExcelUnity.Exporter.Styles;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ExcelUnity.Exporter.Exporters
{
    public class ExportSheet
    {
        public ExportSheet(IEnumerable<object> data)
        {
            Data = data.ToList();
            DataType = data.GetType().GetGenericArguments()[0];
        }

        public ExportSheet(DataTable data)
        {
            Table = data;
            DataType = typeof(DataTable);
        }
        /// <summary>
        /// 第一行所在行号（包含title）
        /// </summary>
        public int? StartRowIndex { get; set; }
        /// <summary>
        /// 数据源:列表
        /// </summary>
        public List<object>? Data { get; }

        /// <summary>
        /// 数据源：datatable
        /// </summary>
        public DataTable? Table { get; }

        /// <summary>
        /// 导出数据的sheet名字,如果名字相同，则合并到同一个sheet
        /// </summary>
        public string? SheetName { get; set; }

        /// <summary>
        /// 导出数据的title
        /// </summary>
        public ExportSheetTitle? Title { get; set; }

        /// <summary>
        /// 导出的数据类型
        /// </summary>
        public Type DataType { get; }

        /// <summary>
        /// 水印
        /// </summary>
        public ExportSheetWarterMark WaterMark { get; set; }

        public ExportMergeConfig MergeConfig { get; set; }


        /// <summary>
        /// 根据列索引设置整表头样式
        /// </summary>
        public Dictionary<int, BaseStyle> HeaderStyleByIndex { get; set; }
        
        /// <summary>
        /// 根据列名设置整表头样式
        /// </summary>
        public Dictionary<string, BaseStyle> HeaderStyleByName { get; set; }

        /// <summary>
        /// 根据列名设置整列样式
        /// </summary>
        public Dictionary<string, BaseStyle> ColumnStyleByName { get; set; }

        /// <summary>
        /// 根据列索引设置整列样式
        /// </summary>
        public Dictionary<int, BaseStyle> ColumnStyleByIndex { get; set; }

        /// <summary>
        /// 根据列名设置整列大小
        /// </summary>
        public Dictionary<string, ExportColumnSize> ColumnSizeByName { get; set; }

        /// <summary>
        /// 根据列索引设置整列大小
        /// </summary>
        public Dictionary<int, ExportColumnSize> ColumnSizeByIndex { get; set; }

        /// <summary>
        /// 单独给某个单元格设置样式
        /// key：列index，列值，style
        /// </summary>
        public Dictionary<int, Dictionary<string, BaseStyle>> SpecialCellStyleByColumnIndex { get; private set; }

        /// <summary>
        /// 单独给某个单元格设置样式
        /// key：列导出名，列值，style
        /// </summary>
        public Dictionary<string, Dictionary<string, BaseStyle>> SpecialCellStyleByColumnName { get; private set; }

        /// <summary>
        /// 单独给某个单元格设置样式
        /// </summary>
        /// <param name="index">列index</param>
        /// <param name="value">列值</param>
        /// <param name="style">style</param>
        public ExportSheet SetSpecialCellStyleByColumnIndex(int index, string value, BaseStyle style)
        {
            SpecialCellStyleByColumnIndex ??= new Dictionary<int, Dictionary<string, BaseStyle>>();
            if (!SpecialCellStyleByColumnIndex.ContainsKey(index))
                SpecialCellStyleByColumnIndex.Add(index, new Dictionary<string, BaseStyle>());
            SpecialCellStyleByColumnIndex[index].Add(value, style);
            return this;
        }

        /// <summary>
        /// 单独给某个单元格设置样式
        /// </summary>
        /// <param name="name">列导出名</param>
        /// <param name="value">列值</param>
        /// <param name="style">style</param>
        public ExportSheet SetSpecialCellStyleByColumnName(string name, string value, BaseStyle style)
        {
            SpecialCellStyleByColumnName ??= new Dictionary<string, Dictionary<string, BaseStyle>>();
            if (!SpecialCellStyleByColumnName.ContainsKey(name))
                SpecialCellStyleByColumnName.Add(name, new Dictionary<string, BaseStyle>());
            SpecialCellStyleByColumnName[name].Add(value, style);
            return this;
        }
    }
}
