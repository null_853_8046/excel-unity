﻿using System.Collections.Generic;

namespace ExcelUnity.Exporter.Exporters
{
    public class ExportMergeConfig
    {
        public List<string> MergeRowAloneByName { get; set; }

        public List<int> MergeRowAloneByIndex { get; set; }

        public List<string> PrimaryKeyByName { get; set; }

        public List<int> PrimaryKeyByIndex { get; set; }

        public List<string> MergeRowByPrimaryByName { get; set; }

        public List<int> MergeRowByPrimaryByIndex { get; set; }
    }
}
