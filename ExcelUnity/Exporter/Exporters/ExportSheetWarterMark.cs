﻿using System.Drawing;

namespace ExcelUnity.Exporter.Exporters
{
    /// <summary>
    /// 水印
    /// </summary>
    public class ExportSheetWarterMark
    {
        public ExportSheetWarterMark(string content)
        {
            Content = content;
        }

        public ExportSheetWarterMark(byte[] image)
        {
            ImageData = image;
        }
        public string Content { get; set; }

        /// <summary>
        /// 单个图片宽
        /// </summary>
        public int Width { get; set; } = 200;

        /// <summary>
        /// 单个图片高
        /// </summary>
        public int Height { get; set; } = 200;

        public Font Font { get; set; } = new Font("", 18);

        /// <summary>
        /// 图片文字颜色
        /// </summary>
        public Color TextColor { get; set; } = Color.LightGray;

        /// <summary>
        /// 图片背景色
        /// </summary>
        public Color BackColor { get; set; } = Color.White;

        /// <summary>
        /// 直接给image将会覆盖以上配置
        /// </summary>
        public byte[] ImageData { get; set; }
    }
}
