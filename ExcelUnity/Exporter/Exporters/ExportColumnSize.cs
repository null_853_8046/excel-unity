﻿namespace ExcelUnity.Exporter.Exporters
{
    public class ExportColumnSize
    {
        /// <summary>
        /// 单元格最小宽度
        /// </summary>
        public int? MinWidth { get; set; }

        /// <summary>
        /// 单元格最大宽度
        /// </summary>
        public int? MaxWidth { get; set; }
    }
}
