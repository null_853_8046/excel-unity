﻿using ExcelUnity.Core;
using System.Collections.Generic;

namespace ExcelUnity.Exporter.Exporters
{
    public class ExportBook
    {
        public ExportBook(params ExportSheet[] sheets)
        {
            Sheets = sheets;
            FileType = FileType.Xlsx;
        }

        /// <summary>
        /// 导出excel包含的sheet
        /// </summary>
        public IEnumerable<ExportSheet> Sheets { get; set; }

        public FileType FileType { get; set; } = FileType.Xlsx;
    }
}
