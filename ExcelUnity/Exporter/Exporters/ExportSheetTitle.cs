﻿using ExcelUnity.Exporter.Styles;

namespace ExcelUnity.Exporter.Exporters
{
    /// <summary>
    /// 导出的第一行标题
    /// </summary>
    public class ExportSheetTitle
    {
        public ExportSheetTitle(string title)
        {
            Title = title;
        }

        public string Title { get; set; }

        public BaseStyle? Style { get; set; }
    }
}
