﻿using ExcelUnity.Exporter.Styles;
using System.Collections.Generic;

namespace ExcelUnity.Exporter.Wrappers
{
    public class ColumnAdapter
    {
        /// <summary>
        /// 单元格列索引
        /// </summary>
        public int ColumnIndex { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 是否是主键列
        /// </summary>
        public bool IsPrimaryColumn { get; set; }

        /// <summary>
        /// 列合并,相邻的列都为true则合并
        /// </summary>
        public bool MergeColumn { get; set; }

        /// <summary>
        /// 根据主键合并行
        /// </summary>
        public bool MergedRowByPrimaryKey { get; set; }

        /// <summary>
        /// 不根据主键合并行
        /// </summary>
        public bool MergedRowAlone { get; set; }

        /// <summary>
        /// 单元格最小宽度
        /// </summary>
        public int? MinWidth { get; set; }

        /// <summary>
        /// 单元格最大宽度
        /// </summary>
        public int? MaxWidth { get; set; }

        /// <summary>
        /// 表头样式
        /// </summary>
        public BaseStyle? HeaderStyle { get; set; }

        /// <summary>
        /// 整列样式
        /// </summary>
        public BaseStyle? ColumnStyle { get; set; }

        /// <summary>
        /// 格式换内容
        /// </summary>
        public string? StringFormat { get; set; }

        /// <summary>
        /// 当值等于key时的特殊style
        /// value,style
        /// </summary>
        public Dictionary<string, BaseStyle> SpecialCellStyle { get; set; }
    }
}
