﻿using ExcelUnity.Exporter.Exporters;
using System.Collections.Generic;

namespace ExcelUnity.Exporter.Wrappers
{
    public static class ExportSheetExtension
    {
        public static List<ColumnAdapter> ToAdapters(this ExportSheet sheet, Dictionary<int, string> columns)
        {
            var ret = new List<ColumnAdapter>();
            foreach (var column in columns)
            {
                var adapter = sheet.ToAdapter(column.Key, column.Value);
                ret.Add(adapter);
            }
            return ret;
        }

        public static ColumnAdapter ToAdapter(this ExportSheet sheet, int columnIndex, string columnName)
        {
            var adapter = new ColumnAdapter
            {
                ColumnIndex = columnIndex,
                Name = columnName
            };

            if (sheet.MergeConfig != null)
            {
                var mergeConfig = sheet.MergeConfig;
                adapter.MergedRowAlone = (mergeConfig.MergeRowAloneByName?.Contains(columnName) ?? false) ||
                                         (mergeConfig.MergeRowAloneByIndex?.Contains(columnIndex) ?? false);

                adapter.MergedRowByPrimaryKey = (mergeConfig.MergeRowByPrimaryByName?.Contains(columnName) ?? false) ||
                                                (mergeConfig.MergeRowByPrimaryByIndex?.Contains(columnIndex) ?? false);

                adapter.IsPrimaryColumn = (mergeConfig.PrimaryKeyByName?.Contains(columnName) ?? false) ||
                                          (mergeConfig.PrimaryKeyByIndex?.Contains(columnIndex) ?? false);
            }

            if (sheet.ColumnStyleByIndex != null && sheet.ColumnStyleByIndex.ContainsKey(columnIndex))
                adapter.ColumnStyle = sheet.ColumnStyleByIndex[columnIndex];
            else if (sheet.ColumnStyleByName != null && sheet.ColumnStyleByName.ContainsKey(columnName))
                adapter.ColumnStyle = sheet.ColumnStyleByName[columnName];

            if (sheet.HeaderStyleByIndex != null && sheet.HeaderStyleByIndex.ContainsKey(columnIndex))
                adapter.HeaderStyle = sheet.HeaderStyleByIndex[columnIndex];
            else if (sheet.HeaderStyleByName != null && sheet.HeaderStyleByName.ContainsKey(columnName))
                adapter.HeaderStyle = sheet.HeaderStyleByName[columnName];

            if (sheet.ColumnSizeByIndex != null && sheet.ColumnSizeByIndex.ContainsKey(columnIndex))
            {
                adapter.MinWidth = sheet.ColumnSizeByIndex[columnIndex].MinWidth;
                adapter.MaxWidth = sheet.ColumnSizeByIndex[columnIndex].MaxWidth;
            }
            else if (sheet.ColumnSizeByName != null && sheet.ColumnSizeByName.ContainsKey(columnName))
            {
                adapter.MinWidth = sheet.ColumnSizeByName[columnName].MinWidth;
                adapter.MaxWidth = sheet.ColumnSizeByName[columnName].MaxWidth;
            }

            if (sheet.SpecialCellStyleByColumnIndex != null && sheet.SpecialCellStyleByColumnIndex.ContainsKey(columnIndex))
                adapter.SpecialCellStyle = sheet.SpecialCellStyleByColumnIndex[columnIndex];
            else if (sheet.SpecialCellStyleByColumnName != null && sheet.SpecialCellStyleByColumnName.ContainsKey(columnName))
                adapter.SpecialCellStyle = sheet.SpecialCellStyleByColumnName[columnName];

            return adapter;
        }
    }
}
