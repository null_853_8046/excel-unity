﻿using ExcelUnity.Core;
using System;
using System.Reflection;

namespace ExcelUnity.Exporter.Wrappers
{
    public class ColumnProperty
    {

        public ColumnAdapter ColumnAdapter { get; set; }

        /// <summary>
        /// 列对应的对象属性
        /// </summary>
        public PropertyInfo PropertyInfo { get; set; }

        /// <summary>
        /// 获取该属性在cell中展示的文本
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public string GetCellValue(object target)
        {
            var objectValue = PropertyInfo.GetValue(target);

            string cellValue = string.Empty;
            if (objectValue != null)
            {
                if (PropertyInfo.PropertyType == typeof(DateTime) || PropertyInfo.PropertyType == typeof(DateTime?))
                {
                    var format = ColumnAdapter.StringFormat.IsNullOrWhiteSpace() ? ExportConfig.DefaultDateFormat : ColumnAdapter.StringFormat;
                    cellValue = ((DateTime)objectValue).ToString(format);
                }
                else
                {
                    cellValue = objectValue.ToString();
                }
            }
            return cellValue;
        }
    }
}