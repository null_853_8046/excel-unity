﻿using ExcelUnity.Exporter.Exporters;

namespace ExcelUnity.Exporter.Wrappers.SheetWrapperBuilders
{
    public class ExportSheetWrapperBuilder
    {
        public static ExportSheetWrapper? BuildSheetWrapper(ExportSheet exportBookSheet)
        {
            ExportSheetWrapper? sheetWrapper;

            if (exportBookSheet.Table != null)
                sheetWrapper = new DataTableBuilder().BuildExportSheetWrapper(exportBookSheet);
            else if (exportBookSheet.DataType.FullName == "System.Object")
                sheetWrapper = new DynamicTypeBuilder().BuildExportSheetWrapper(exportBookSheet);
            else
                sheetWrapper = new DefaultTypeBuilder().BuildExportSheetWrapper(exportBookSheet);

            return sheetWrapper;
        }
    }
}
