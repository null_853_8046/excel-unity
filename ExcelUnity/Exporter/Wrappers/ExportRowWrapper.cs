﻿using System.Collections.Generic;
using System.Linq;

namespace ExcelUnity.Exporter.Wrappers
{
    public class ExportRowWrapper
    {
        public ExportRowWrapper(List<ExportCellWrapper> cells, int rowIndex)
        {
            Cells = cells;
            RowIndex = rowIndex;
            PrimaryKey = string.Join("_", Cells.Where(x => x.IsPrimaryKeyColumn).Select(x => x.Value));
        }

        /// <summary>
        /// 数据所在的excel行索引
        /// </summary>
        public int RowIndex { get; }

        /// <summary>
        /// 行所包含的单元格
        /// </summary>
        public List<ExportCellWrapper> Cells { get; set; } = new List<ExportCellWrapper>();

        /// <summary>
        /// 唯一键
        /// </summary>
        public string? PrimaryKey { get; }
    }
}