﻿using ExcelUnity.Exporter.Exporters;
using System.Collections.Generic;

namespace ExcelUnity.Exporter.Wrappers
{
    public class ExportSheetWrapper
    {
        public ExportSheetWrapper(List<ExportColumnWrapper> columns, List<ExportRowWrapper> rows, ExportSheetWarterMark waterMark)
        {
            Columns = columns;
            Rows = rows;
            WaterMark = waterMark;
        }

        public List<ExportColumnWrapper> Columns { get; set; }

        public List<ExportRowWrapper> Rows { get; set; }

        /// <summary>
        /// 水印
        /// </summary>
        public ExportSheetWarterMark WaterMark { get; set; }
    }
}
