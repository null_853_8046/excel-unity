﻿using ExcelUnity.Exporter.Styles;

namespace ExcelUnity.Exporter.Wrappers
{
    public class ExportCellWrapper
    {
        public ExportCellWrapper(int columnIndex, string value, BaseStyle baseStyle)
        {
            ColumnIndex = columnIndex;
            Value = value;
            CellStyle = baseStyle;
        }

        public ExportCellWrapper(string value, ColumnAdapter adapter)
        {
            Value = value;
            ColumnIndex = adapter.ColumnIndex;
            MergedRowAlone = adapter.MergedRowAlone;
            MergedRowByPrimaryKey = adapter.MergedRowByPrimaryKey;
            IsPrimaryKeyColumn = adapter.IsPrimaryColumn;
            CellStyle = adapter.ColumnStyle;
        }

        /// <summary>
        /// 单元格列索引
        /// </summary>
        public int ColumnIndex { get; set; }

        /// <summary>
        /// 单元格内容
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 是否是主键列
        /// </summary>
        public bool IsPrimaryKeyColumn { get; set; }

        /// <summary>
        /// 列合并,相邻的列都为true则合并
        /// </summary>
        public bool MergeColumn { get; set; }

        /// <summary>
        /// 根据主键合并行
        /// </summary>
        public bool MergedRowByPrimaryKey { get; set; }

        /// <summary>
        /// 不根据主键合并行
        /// </summary>
        public bool MergedRowAlone { get; set; }

        public BaseStyle? CellStyle { get; set; }
    }
}
