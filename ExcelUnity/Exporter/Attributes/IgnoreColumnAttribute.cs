﻿using System;

namespace ExcelUnity.Exporter.Attributes
{
    /// <summary>
    /// 不导出行
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class IgnoreColumnAttribute : Attribute
    {
    }
}
