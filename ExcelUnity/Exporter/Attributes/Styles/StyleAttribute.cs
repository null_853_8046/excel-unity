﻿using ExcelUnity.Exporter.Styles;
using System;

namespace ExcelUnity.Exporter.Attributes.Styles
{
    public class StyleAttribute : Attribute
    {
        public StyleAttribute(bool isBold = false,
            bool wrapText = false,
            short fontColor = 8,
            int fontSize = 11,
            string fontName = "宋体",
            short fillForegroundColor = -1,
            HorizontalAlign horizontalAlign = HorizontalAlign.Left,
            VerticalAlignment verticalAlign = VerticalAlignment.Center)
        {
            Style = CreateStyle();
            Style.IsBold = isBold;
            Style.WrapText = wrapText;
            Style.FontColor = fontColor;
            Style.FontSize = fontSize;
            Style.FontName = fontName;
            if (fillForegroundColor > 0)
                Style.FillForegroundColor = fillForegroundColor;
            Style.HorizontalAlign = horizontalAlign;
            Style.VerticalAlign = verticalAlign;
        }
        public virtual BaseStyle CreateStyle()
        {
            return new BaseStyle();
        }
        public BaseStyle Style { get; set; }
    }
}