﻿using ExcelUnity.Exporter.Styles;
using System;

namespace ExcelUnity.Exporter.Attributes.Styles
{
    /// <summary>
    /// 表头样式
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class HeaderStyleAttribute : StyleAttribute
    {
        public HeaderStyleAttribute(bool isBold = true, bool wrapText = false, short fontColor = 8, int fontSize = 11, string fontName = "宋体", short fillForegroundColor = -1, HorizontalAlign horizontalAlign = HorizontalAlign.Center, VerticalAlignment verticalAlign = VerticalAlignment.Center) : base(isBold, wrapText, fontColor, fontSize, fontName, fillForegroundColor, horizontalAlign, verticalAlign)
        {
        }
    }
}